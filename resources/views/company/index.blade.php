@extends('layout.master')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body d-flex justify-content-between">
                    <h4>
                        List Company
                    </h4>
                    <a href="/company/create" class="btn btn-sm btn-primary rounded-pill">Tambah Data Baru</a>
                </div>
            </div>
            <div class="card">
                <div class="card-body d-flex justify-content-center">
                <table class="table table-striped">
                <thead>
                    <tr class="text-center">
                    <th scope="col">id</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Alamat</th>
                    <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($companies as $company)
                    <tr class="text-center">
                    <th scope="row">{{$company->id}}</th>
                    <td class="align-middle">{{$company->nama}}</td>
                    <td>{{$company->alamat}}</td>
                    <td class="d-flex justify-content-center">
                        <a class="btn btn-warning btn-sm rounded-pill mr-3" href="/company/{{$company->id}}/edit">Edit</a>
                        <form action="/company/{{$company->id}}/delete" method="POST">
                            @method('delete')
                            @csrf
                            <button type="submit" class="btn btn-danger btn-sm rounded-pill">Delete</button>
                        </form>
                    </td>
                    </tr>
                    @endforeach
                </tbody>
                </table>
                </div>
                <div class="d-flex justify-content-center mb-5">
                    <a href="/download-company-excell" class="btn btn-sm btn-success rounded-pill mx-2">Download Excell</a>
                    <a href="/download-company-pdf" class="btn btn-sm btn-success rounded-pill mx-2">Download PDF</a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
