@extends('layout.master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header d-flex justify-content-center">
                    <h4>
                        Fungsi String-Kalkulator Sederhana
                    </h4>
                </div>
                <div class="card-body d-flex justify-content-center">
                    Maaf, Perhitungan tidak bisa dilakukan (karena pembagian dengan nol)
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
