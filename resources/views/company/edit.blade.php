@extends('layout.master')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <div class="text-center mb-3">
                        <h5>Edit Data Company</h5>
                    </div>
                    <div class="row">
                        <div class="col-8">
                            <form action="/company/{{$companies->id}}/edit" method="POST">
                                @method('patch')
                                @csrf
                                <div class="form-group mb-3">
                                  <label for="">Nama :</label>
                                  <input type="text" class="form-control" value="{{old('nama') ?? $companies->nama }}" id="nama" name="nama" placeholder="Masukkan Nama Company">
                                </div>
                                <div class="form-group mb-3">
                                  <label for="">Alamat :</label>
                                  <input type="text" class="form-control" value="{{old('alamat')?? $companies->alamat }}" id="alamat" name="alamat" placeholder="Masukkan Alamat">
                                </div>
                                <button type="submit" class="btn btn-primary rounded-pill">Update</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
