@extends('layout.master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header d-flex justify-content-center">
                    <h4>
                        Fungsi String-Kalkulator Sederhana
                    </h4>
                </div>
                <div class="card-body d-flex justify-content-center">
                    <form action="/kalkulator/store" method="POST">
                        @csrf
                        <div class="mb-3">
                            <label for="bil1" class="form-label">Masukkan Bilangan Pertama :</label>
                            <input type="text" name="bil1" class="form-control" id="bil1">
                            <label for="category">Operasi</label>
                            <select name="category" id="category" class="form-control">
                                <option value="" selected disabled>Pilih Operasi :</option>
                                <option value="+"> + (penjumlahan)</option>
                                <option value="-"> - (pengurangan)</option>
                                <option value="x"> * (perkalian)</option>
                                <option value="/"> / (pembagian)</option>
                            </select>
                            <label for="bil2" class="form-label">Masukkan Bilangan Kedua :</label>
                            <input type="text" name="bil2" class="form-control" id="bil2">
                        </div>
                        <button type="submit" class="btn btn-primary rounded-pill">Hitung</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
