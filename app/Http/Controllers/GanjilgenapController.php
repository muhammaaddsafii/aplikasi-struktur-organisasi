<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GanjilgenapController extends Controller
{
    public function index()
    {
        return view('ganjil-genap');
    }

    public function store(Request $request)
    {
        $bil1 = $request->bil1;
        $bil2 = $request->bil2;
        $arrray = [];

        if ($bil1 > $bil2) {
            return view('ganjil-genap-hasil-null');
        } else {
            for ($i = $bil1; $i <= $bil2; $i++) {
                if ($i % 2 == 0) {
                    array_push($arrray, "Angka $i adalah bilangan genap");
                } else {
                    array_push($arrray, "Angka $i adalah bilangan ganjil");
                }
            }
        }
        return view('ganjil-genap-hasil', ['array' => $arrray]);
    }
}
