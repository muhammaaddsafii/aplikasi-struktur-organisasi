@if (session()->has('success'))
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="alert alert-success">
                {{session()->get('success')}}
            </div>
        </div>
    </div>
</div>
@endif
@if (session()->has('error'))
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="alert alert-danger">
                {{session()->get('error')}}
            </div>
        </div>
    </div>
</div>
@endif
