@extends('layout.master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header d-flex justify-content-center">
                    <h4>
                        Fungsi Huruf Vocal
                    </h4>
                </div>
                <div class="card-body d-flex justify-content-center">
                    Jumlah huruf vocal "{{$string}}" ada {{$hitung}}, yaitu
                    @foreach ($unique as $unik)
                    {{$unik}}
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
