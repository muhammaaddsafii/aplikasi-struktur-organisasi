<table class="table table-bordered">
    <thead>
      <tr>
        <th scope="col">id</th>
        <th scope="col">Nama</th>
        <th scope="col">Posisi</th>
        <th scope="col">Perusahaan</th>
      </tr>
    </thead>
    <tbody>
        @foreach ($company as $company)
        <tr>
        <th scope="row">{{ $company->id }}</th>
        <td>{{ $company->nama }}</td>
        <td>{{ $company->alamat }}</td>
        </tr>
        @endforeach
    </tbody>
  </table>
