<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HurufvocalController extends Controller
{
    public function index()
    {
        return view('huruf-vocal');
    }

    public function store(Request $request)
    {
        $string = $request->string;
        $vocal = ['a', 'i', 'u', 'e', 'o'];
        $arr = str_split($string);

        $found = array_intersect($arr, $vocal);
        $unique = array_unique($found);

        $hitung = count($unique);

        return view('huruf-vocal-hasil', [
            'string' => $string,
            'hitung' => $hitung,
            'unique' => $unique
        ]);
    }
}
