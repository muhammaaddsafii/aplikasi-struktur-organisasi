@extends('layout.master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header d-flex justify-content-center">
                    <h4>
                        Fungsi String-Kalkulator Sederhana
                    </h4>
                </div>
                <div class="card-body d-flex justify-content-center">
                    Hasil dari {{$bil1}} {{$category}} {{$bil2}} adalah {{$hitung}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
