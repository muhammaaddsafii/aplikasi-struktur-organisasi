@extends('layout.master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header d-flex justify-content-center">
                    <h4>
                        Fungsi Huruf Vocal
                    </h4>
                </div>
                <div class="card-header d-flex justify-content-center">
                    <p class="text-secondary">
                        Disini kita akan memberikan input suatu string, dan sistem akan mencari berapa jumlah huruf vocalnya. Ketika terdapat duplikat huruf vocal, maka hanya akan dihitung satu saja. <br><br>
                        input harus diisi dengan <b>lowercase.</b> <br><br>
                        Contoh Input : "selamat datang" <br><br>
                        Output : Jumlah Huruf vocal ada 2, yaitu e dan a. <br>
                    </p>
                </div>
                <div class="card-body d-flex justify-content-center">
                    <form action="/huruf-vocal/store" method="POST">
                        @csrf
                        <div class="mb-3">
                            <label for="string" class="form-label">Masukkan String :</label>
                            <input type="text" name="string" class="form-control" id="bil1">
                        </div>
                        <button type="submit" class="btn btn-primary rounded-pill">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
