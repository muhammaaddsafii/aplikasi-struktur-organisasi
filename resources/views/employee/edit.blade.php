@extends('layout.master')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <div class="text-center mb-3">
                        <h5>Edit Data</h5>
                    </div>
                    <div class="row">
                        <div class="col-8">
                            <form action="/employee/{{$employee->id}}/edit" method="POST">
                                @method('patch')
                                @csrf
                                <div class="form-group mb-3">
                                  <label for="">Nama :</label>
                                  <input type="text" class="form-control" id="nama" name="nama" value="{{ old('nama') ?? $employee->nama }}" placeholder="Masukkan Nama">
                                </div>
                                <div class="form-group mb-3">
                                  <label for="">Id Atasan :</label>
                                  <input type="text" class="form-control" id="atasan_id" name="atasan_id" value="{{ old('atasan_id') ?? $employee->atasan_id }}" placeholder="Masukkan Id Atasan">
                                </div>
                                <div class="form-group mb-3">
                                  <label for="">Id Company :</label>
                                  <input type="text" class="form-control" id="company_id" name="company_id" value="{{ old('company_id') ?? $employee->company_id }}" placeholder="Masukkan Id Company">
                                </div>
                                <button type="submit" class="btn btn-primary rounded-pill">Update</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
